FROM openjdk:8-jdk-buster
LABEL autor="Samuel Moreira"
LABEL email="samuelmoreira@hotmail.es"
LABEL project="inventario"
LABEL company="Kruger"
RUN rm /etc/localtime && \
ln -s /usr/share/zoneinfo/America/Guayaquil /etc/localtime && \
mkdir -p app/build/config && \
mkdir -p app/build/log && \
chmod 777 app
ADD target/*.jar app.jar
ADD src/main/resources/application.properties app/build/config/application.properties
EXPOSE 2001
ENTRYPOINT ["java","-XX:+UseG1GC","-Dfile.encoding=UTF-8","-Duser.timezone=America/Guayaquil","-jar","/app.jar","--spring.config.location=file:/app/build/config/application.properties"]
