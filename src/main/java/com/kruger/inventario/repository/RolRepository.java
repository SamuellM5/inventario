package com.kruger.inventario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.entity.RolesEntity;

/**
 * Repository de Rol
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Repository
public interface RolRepository extends JpaRepository<RolesEntity, Long> {

	List<RolesEntity> findByCodeAndStatus(String code, String status);

}
