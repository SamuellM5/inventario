package com.kruger.inventario.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.entity.UserEntity;

/**
 * Repository de Usuario
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

	List<UserEntity> findByDniAndStatus(String dni, String status);

	@Query(nativeQuery = true, value = "select u.* from vacuna as v, usuario as u \r\n"
			+ "where v.usuario_id = u.id_usuario\r\n" + "and u.estado_vacuna = ?1\r\n"
			+ "and v.fecha_vacuna BETWEEN ?2 and ?3\r\n" + "and v.nombre_vacuna ilike ?4\r\n"
			+ "or  u.estado_vacuna = ?1;")
	public List<UserEntity> queryFiltro(Boolean vaccinestatus, Date timeStart, Date timeEnd, String tipovacuna);

}
