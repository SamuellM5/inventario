package com.kruger.inventario.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.entity.UserRol;

/**
 * Repository de Usuario Rol
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Repository
public interface UserRolRepository extends JpaRepository<UserRol, Long> {

}
