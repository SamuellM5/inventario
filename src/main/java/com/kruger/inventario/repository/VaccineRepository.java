package com.kruger.inventario.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kruger.inventario.entity.VaccineEntity;

/**
 * Repository de vacuna
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Repository
public interface VaccineRepository extends JpaRepository<VaccineEntity, Long> {

	List<VaccineEntity> findByCodeAndStatus(String code, String satus);

}
