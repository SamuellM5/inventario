package com.kruger.inventario.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.ResponseFind;
import com.kruger.inventario.dto.User;
import com.kruger.inventario.service.UserService;

@CrossOrigin("*")
@RestController
@RequestMapping("/user/")
public class UserApi {

	@Autowired
	private UserService userService;

	/**
	 * Interfaz del metodo que permitetene crear usuario
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PostMapping("/create")
	public ResponseEntity<Response> create(@RequestBody User user) {

		return ResponseEntity.ok(userService.create(user));

	}

	/**
	 * Interfaz del metodo que permitetene actualizar usuario
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PutMapping("/update")
	public ResponseEntity<Response> update(@RequestBody(required = true) User user) {

		return ResponseEntity.ok(userService.update(user));

	}

	/**
	 * Interfaz del metodo que permitetene eliminar usuario
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@DeleteMapping("/delete")
	public ResponseEntity<Response> delete(String user, String dni) {

		return ResponseEntity.ok(userService.delete(user, dni));

	}

	/**
	 * Interfaz del metodo que permitetene listar usuarios
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@GetMapping("/find")

	public ResponseEntity<ResponseFind> find(Boolean vaccinestatus, Date timeStart, Date timeEnd, String tipovacuna)

	{
		return ResponseEntity.ok(userService.find(vaccinestatus, timeStart, timeEnd, tipovacuna)); // ResponseEntity.ok(userService.find(status));

	}

}
