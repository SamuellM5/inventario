package com.kruger.inventario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.Rol;
import com.kruger.inventario.service.RolService;

@CrossOrigin("*")
@RestController
@RequestMapping("/rol/")
public class RolApi {

	@Autowired
	private RolService rolservice;

	/**
	 * Interfaz del metodo que permitetene crear rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PostMapping("/create")
	public ResponseEntity<Response> create(@RequestBody Rol rol) {

		return ResponseEntity.ok(rolservice.create(rol));

	}

	/**
	 * Interfaz del metodo que permitetene actualizar rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PutMapping("/update")
	public ResponseEntity<Response> update(@RequestBody(required = true) Rol rol) {

		return ResponseEntity.ok(rolservice.update(rol));

	}

	/**
	 * Interfaz del metodo que permitetene eliminar rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */
	@DeleteMapping("/delete")
	public ResponseEntity<Response> delete(String rol, String code) {

		return ResponseEntity.ok(rolservice.delete(rol, code));

	}
}