package com.kruger.inventario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.Vaccine;
import com.kruger.inventario.service.VaccineService;

@CrossOrigin("*")
@RestController
@RequestMapping("/vacuna/")
public class VaccineApi {

	@Autowired
	private VaccineService vaccineService;

	/**
	 * Interfaz del metodo que permitetene crear vacunacion
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PostMapping("/create")
	public ResponseEntity<Response> create(@RequestBody Vaccine vaccine) {

		return ResponseEntity.ok(vaccineService.create(vaccine));

	}

	/**
	 * Interfaz del metodo que permitetene actualizar vacunacion
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@PutMapping("/update")
	public ResponseEntity<Response> update(@RequestBody(required = true) Vaccine vaccine) {

		return ResponseEntity.ok(vaccineService.update(vaccine));

	}

	/**
	 * Interfaz del metodo que permitetene eliminar vacunacion
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2020
	 *
	 */

	@DeleteMapping("/delete")
	public ResponseEntity<Response> delete(String vaccine, String code) {

		return ResponseEntity.ok(vaccineService.delete(vaccine, code));

	}
}
