package com.kruger.inventario;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackages = { "com.kruger.inventario", "com.kruger.inventario.interface" , "com.kruger.inventario.config", "com.kruger.inventario.entity", "com.kruger.inventario.repositories", "com.kruger.inventario.model","com.kruger.inventario.service"})
public class InventarioApplication {

	public static void main(String[] args) {
		SpringApplication.run(InventarioApplication.class, args);
	}

}
