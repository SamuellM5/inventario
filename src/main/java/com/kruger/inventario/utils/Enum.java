package com.kruger.inventario.utils;

/**
 * Enum se configura response 
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

public enum Enum {

		CREATED(100, "Created"), 
		NOT_CREATED(101, "Not created"), 
		UPDATED(102, "Updated"), 
		NOT_UPDATED(103, "Not updated"),
		FOUND(104, "Found"), 
		NOT_FOUND(105, "Not found"), 
		DELETED(106, "Deleted"), 
		NOT_DELETED(107, "Not deleted"),
		FAILED(108, "Failed"),
		GENERATED(109, "Generated"),
		NONE(110, "None"),
		INVALID_STATE(117, "Invalid State"),
		VALID_DATA(120, "Valid Data"),
		INVALID_DATA(121, "Invalid Data"),
		OK(200, "Ok"),
		UNAUTHORIZED(401, "Unauthorized"),
		PROCESSING(111, "Processing"),
		INTERNAL_SERVER_ERROR(500,"Internal Server Error");

		private final Integer value;
		private final String message;
		
		Enum(Integer value, String message) {
			this.value = value;
			this.message = message;
		}

		/**
		 * @return the value
		 */
		public Integer getValue() {
			return value;
		}

		/**
		 * @return the message
		 */
		public String getMessage() {
			return message;
		}
		
		
}
	
	
