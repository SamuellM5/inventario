package com.kruger.inventario.dto;

import java.util.Date;

/**
 * Definición del dto User.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */

public class User {

	private String fisrtname;
	private String lastname;
	private String dni;
	private String email;
	private String password;
	private Date birthday;
	private String address;
	private String phone;
	private String user;
	private String rolCode;

	public String getFisrtname() {
		return fisrtname;
	}

	public void setFisrtname(String fisrtname) {
		this.fisrtname = fisrtname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getRolCode() {
		return rolCode;
	}

	public void setRolCode(String rolCode) {
		this.rolCode = rolCode;
	}

	@Override
	public String toString() {
		return "User [fisrtname=" + fisrtname + ", lastname=" + lastname + ", dni=" + dni + ", email=" + email
				+ ", password=" + password + ", birthday=" + birthday + ", address=" + address + ", phone=" + phone
				+ ", user=" + user + ", rolCode=" + rolCode + "]";
	}

}
