package com.kruger.inventario.dto;

import java.io.Serializable;

import com.kruger.inventario.utils.Enum;

/**
 * Definición del dto de respuesta.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */

public class Response implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer code;
	private String message;
	private String description;

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Response(Response response) {
		super();
		this.code = response.getCode();
		this.message = response.getMessage();
	}

	/**
	 * @param result
	 */
	public Response(Enum result) {
		super();
		this.code = result.getValue();
		this.message = result.getMessage();
	}
}