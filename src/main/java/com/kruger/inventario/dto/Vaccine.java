package com.kruger.inventario.dto;

import java.util.Date;

/**
 * Definición del dto vacuna.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */

public class Vaccine {

	private String vaccinename;
	private Date vaccineDate;
	private String numberdoses;
	private String user;
	private String userDni;
	private String code;

	public String getVaccinename() {
		return vaccinename;
	}

	public void setVaccinename(String vaccinename) {
		this.vaccinename = vaccinename;
	}

	public Date getVaccineDate() {
		return vaccineDate;
	}

	public void setVaccineDate(Date vaccineDate) {
		this.vaccineDate = vaccineDate;
	}

	public String getNumberdoses() {
		return numberdoses;
	}

	public void setNumberdoses(String numberdoses) {
		this.numberdoses = numberdoses;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getUserDni() {
		return userDni;
	}

	public void setUserDni(String userDni) {
		this.userDni = userDni;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Vaccine [vaccinename=" + vaccinename + ", vaccineDate=" + vaccineDate + ", numberdoses=" + numberdoses
				+ ", user=" + user + ", userDni=" + userDni + ", code=" + code + "]";
	}

}
