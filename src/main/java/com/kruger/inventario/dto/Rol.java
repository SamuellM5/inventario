package com.kruger.inventario.dto;

/**
 * Definición del dto Rol.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */
public class Rol {

	private String rol;
	private String user;

	private String code;

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "Rol [rol=" + rol + ", user=" + user + ", code=" + code + "]";
	}

}
