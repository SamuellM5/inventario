package com.kruger.inventario.dto;

import java.util.List;

import com.kruger.inventario.utils.Enum;

/**
 * Definición del dto de respuesta de busqueda.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */

public class ResponseFind extends Response {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<User> data;

	public List<User> getData() {
		return data;
	}

	public void setData(List<User> data) {
		this.data = data;
	}

	public ResponseFind(Response response, List<User> data) {
		super(response);
		this.data = data;
	}

	public ResponseFind(Enum result) {
		super(result);
		// TODO Auto-generated constructor stub
	}

	public ResponseFind(Response response) {
		super(response);
		// TODO Auto-generated constructor stub
	}

}
