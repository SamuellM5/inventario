package com.kruger.inventario.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.ResponseFind;
import com.kruger.inventario.dto.User;
import com.kruger.inventario.entity.UserEntity;
import com.kruger.inventario.entity.UserRol;
import com.kruger.inventario.repository.RolRepository;
import com.kruger.inventario.repository.UserRepository;
import com.kruger.inventario.repository.UserRolRepository;
import com.kruger.inventario.utils.Enum;

/**
 * Clase que provee metodos de crud de usuario
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */
@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RolRepository rolRepository;

	@Autowired
	private UserRolRepository userRolRepository;

	/**
	 * Método que permite creacion de usuario
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response create(User user) {
		Response response = null;
		try {
			if (user.getDni().length() != 10) {

				response = new Response(Enum.NOT_CREATED);
				response.setDescription("Lo sentimos la cedula es incorrecta");
			} else if (!user.getEmail().contains("@")) {
				response = new Response(Enum.NOT_CREATED);
				response.setDescription("Lo sentimos el email es incorrecto");

			} else if (user.getFisrtname().chars().allMatch(Character::isDigit)) {
				response = new Response(Enum.NOT_CREATED);
				response.setDescription("Lo sentimos nombre no valido");

			} else if (user.getLastname().chars().allMatch(Character::isDigit)) {
				response = new Response(Enum.NOT_CREATED);
				response.setDescription("Lo sentimos apellido no valido");
			} else {

				UserEntity entity = new UserEntity();
				entity.setAddress(user.getAddress());
				entity.setFisrtname(user.getFisrtname());
				entity.setLastname(user.getLastname());
				entity.setBirthday(user.getBirthday());
				entity.setDni(user.getDni());
				entity.setEmail(user.getEmail());
				entity.setPassword(user.getPassword());
				entity.setPhone(user.getPhone());
				entity.setCreationdate(new Date());
				entity.setModificationdate(new Date());
				entity.setCreatoruser(user.getUser());
				entity.setModifieruser(user.getUser());
				entity.setStatus("Activo");

				userRepository.saveAndFlush(entity);

				UserRol userRol = new UserRol();
				userRol.setRol(rolRepository.findByCodeAndStatus(user.getRolCode(), "Activo").stream().findFirst()
						.orElse(null));
				userRol.setUser(entity);
				userRol.setCreationdate(new Date());
				userRol.setModificationdate(new Date());
				userRol.setCreatoruser(user.getUser());
				userRol.setModifieruser(user.getUser());
				userRol.setStatus("Activo");

				userRolRepository.save(userRol);

				response = new Response(Enum.CREATED);
				response.setDescription("Usuario creado correctamente");
			}
		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite listar usuarios
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	private String replace(String b) {
		String a = null;

		a = "%" + b + "%";
		return a;

	}

	public ResponseFind find(Boolean vaccinestatus, Date timeStart, Date timeEnd, String tipovacuna) {
		List<User> users = null;

		ResponseFind response = null;
		try {
			List<UserEntity> userList = userRepository.queryFiltro(vaccinestatus, timeStart, timeEnd,
					this.replace(tipovacuna));

			if (!userList.isEmpty()) {
				users = new ArrayList<>();
				for (UserEntity u : userList) {
					User user = new User();
					user.setAddress(u.getAddress());
					user.setBirthday(u.getBirthday());
					user.setDni(u.getDni());
					user.setEmail(u.getEmail());
					user.setFisrtname(u.getFisrtname());
					user.setLastname(u.getLastname());
					user.setPhone(u.getPhone());

					users.add(user);

				}
				response = new ResponseFind(Enum.FOUND);
				response.setDescription("Usuarios encontrados");
				response.setData(users);

			} else {
				response = new ResponseFind(Enum.NOT_FOUND);
				response.setDescription("Lo sentimos no se encontraron usuarios");
			}
		} catch (Exception e) {
			response = new ResponseFind(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite actualizar usuarios
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response update(User user) {
		Response response = null;
		try {
			UserEntity u = userRepository.findByDniAndStatus(user.getDni(), "Activo").stream().findFirst().orElse(null);
			if (u != null) {
				u.setAddress(user.getAddress());
				u.setBirthday(user.getBirthday());
				u.setEmail(user.getEmail());
				u.setFisrtname(user.getFisrtname());
				u.setLastname(user.getLastname());
				u.setPhone(user.getPhone());
				u.setModificationdate(new Date());
				u.setModifieruser(user.getUser());

				userRepository.save(u);
				response = new Response(Enum.UPDATED);
				response.setDescription("Usuario Actualizado correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido actualizar");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite eliminar usuarios
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response delete(String user, String dni) {
		Response response = null;
		try {
			UserEntity u = userRepository.findByDniAndStatus(dni, "Activo").stream().findFirst().orElse(null);
			if (u != null) {
				u.setStatus("Inactivo");
				u.setModificationdate(new Date());
				u.setModifieruser(user);

				userRepository.save(u);
				response = new Response(Enum.DELETED);
				response.setDescription("Usuario eliminado correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido eliminar el usuario");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

}
