package com.kruger.inventario.service;

import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.Vaccine;
import com.kruger.inventario.entity.VaccineEntity;
import com.kruger.inventario.repository.UserRepository;
import com.kruger.inventario.repository.VaccineRepository;
import com.kruger.inventario.utils.Enum;

/**
 * Clase que provee metodos de crud de vacuna
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */
@Service
@Transactional
public class VaccineService {

	@Autowired
	private VaccineRepository vaccineRepository;

	@Autowired
	private UserRepository userRepository;

	/**
	 * Método que permite creacion de vacuna
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response create(Vaccine vaccine) {
		Response response = null;
		try {

			VaccineEntity entity = new VaccineEntity();
			entity.setCode(UUID.randomUUID().toString());
			entity.setVaccinename(vaccine.getVaccinename());
			entity.setVaccineDate(vaccine.getVaccineDate());
			entity.setNumberdoses(vaccine.getNumberdoses());
			entity.setCreationdate(new Date());
			entity.setModificationdate(new Date());
			entity.setCreatoruser(vaccine.getUser());
			entity.setModifieruser(vaccine.getUser());
			entity.setStatus("Activo");

			entity.setUser(userRepository.findByDniAndStatus(vaccine.getUserDni(), "Activo").stream().findFirst()
					.orElse(null));

			vaccineRepository.save(entity);
			response = new Response(Enum.CREATED);
			response.setDescription("Vacuna creada correctamente");
		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite actualizar vacuna
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response update(Vaccine Vaccine) {
		Response response = null;
		try {
			VaccineEntity v = vaccineRepository.findByCodeAndStatus(Vaccine.getCode(), "Activo").stream().findFirst()
					.orElse(null);
			if (v != null) {
				v.setModificationdate(new Date());
				v.setNumberdoses(Vaccine.getNumberdoses());
				v.setVaccineDate(Vaccine.getVaccineDate());
				v.setVaccinename(Vaccine.getVaccinename());
				v.setModificationdate(new Date());
				v.setModifieruser(Vaccine.getUser());

				vaccineRepository.save(v);
				response = new Response(Enum.UPDATED);
				response.setDescription("Vacuna Actualizada correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido actualizar");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite eliminar vacuna
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */
	public Response delete(String vaccine, String code) {
		Response response = null;
		try {
			VaccineEntity v = vaccineRepository.findByCodeAndStatus(code, "Activo").stream().findFirst().orElse(null);
			if (v != null) {
				v.setStatus("Inactivo");
				v.setModificationdate(new Date());
				v.setModifieruser(vaccine);

				vaccineRepository.save(v);
				response = new Response(Enum.DELETED);
				response.setDescription("Vacuna eliminado correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido eliminar el Rol del usuario");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}
}
