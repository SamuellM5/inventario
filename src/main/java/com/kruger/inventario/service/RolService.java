package com.kruger.inventario.service;

import java.util.Date;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kruger.inventario.dto.Response;
import com.kruger.inventario.dto.Rol;
import com.kruger.inventario.entity.RolesEntity;
import com.kruger.inventario.repository.RolRepository;
import com.kruger.inventario.utils.Enum;

/**
 * Clase que provee metodos de crud de Rol
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */
@Service
@Transactional
public class RolService {

	@Autowired
	private RolRepository rolRepository;

	/**
	 * Método que permite creacion de rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response create(Rol rol) {
		Response response = null;
		try {

			RolesEntity entity = new RolesEntity();
			entity.setRol(rol.getRol());
			entity.setCode(UUID.randomUUID().toString());
			entity.setCreationdate(new Date());
			entity.setCreatoruser(rol.getUser());
			entity.setModificationdate(new Date());
			entity.setModifieruser(rol.getUser());
			entity.setStatus("Activo");

			rolRepository.save(entity);
			response = new Response(Enum.CREATED);
			response.setDescription("Rol creado correctamente");
		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite actualizar rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response update(Rol rol) {
		Response response = null;
		try {
			RolesEntity r = rolRepository.findByCodeAndStatus(rol.getCode(), "Activo").stream().findFirst()
					.orElse(null);
			if (r != null) {
				r.setRol(rol.getRol());
				r.setModificationdate(new Date());
				r.setModifieruser(rol.getUser());

				rolRepository.save(r);
				response = new Response(Enum.UPDATED);
				response.setDescription("Rol Actualizado correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido actualizar");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}

	/**
	 * Método que permite eliminar rol
	 * 
	 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
	 * @version 1.0
	 * @since 13/10/2021
	 *
	 * @return response
	 */

	public Response delete(String rol, String code) {
		Response response = null;
		try {
			RolesEntity r = rolRepository.findByCodeAndStatus(rol, "Activo").stream().findFirst().orElse(null);
			if (r != null) {
				r.setStatus("Inactivo");
				r.setModificationdate(new Date());
				r.setModifieruser(rol);

				rolRepository.save(r);
				response = new Response(Enum.DELETED);
				response.setDescription("Rol eliminado correctamente");
			} else {
				response = new Response(Enum.NOT_UPDATED);
				response.setDescription("Lo sentimos no se ha podido eliminar el Rol del usuario");
			}

		} catch (Exception e) {
			response = new Response(Enum.INTERNAL_SERVER_ERROR);
			response.setDescription("Lo sentimos intente de nuevo mas tarde");
		}
		return response;
	}
}
