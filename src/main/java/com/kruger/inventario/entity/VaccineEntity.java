package com.kruger.inventario.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Definición de la entidad vacuna creacion de tablas y columnas
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Entity
@Table(name = VaccineEntity.TABLE_NAME)
public class VaccineEntity {

	protected static final String TABLE_NAME = "VACUNA";
	protected static final String SEQUENCE = "SEQ_VACUNA";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	@Column(name = "id_vaccine")
	private Long vaccineId;
	@Column(name = "nombreVacuna")
	private String vaccinename;
	@Column(name = "fechaVacuna")
	private Date vaccineDate;
	@Column(name = "numeroDeDosis")
	private String numberdoses;
	@Column(name = "fecha_creacion")
	private Date creationdate;
	@Column(name = "fecha_modificacion")
	private Date modificationdate;
	@Column(name = "usuario_creador")
	private String creatoruser;
	@Column(name = "usuario_modificador")
	private String modifieruser;
	@Column(name = "estado")
	private String status;
	@Column(name = "codigo")
	private String code;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "FK_USUARIO_VACUNA"), nullable = true)
	private UserEntity user;

	public Long getVaccineId() {
		return vaccineId;
	}

	public void setVaccineId(Long vaccineId) {
		this.vaccineId = vaccineId;
	}

	public String getVaccinename() {
		return vaccinename;
	}

	public void setVaccinename(String vaccinename) {
		this.vaccinename = vaccinename;
	}

	public Date getVaccineDate() {
		return vaccineDate;
	}

	public void setVaccineDate(Date vaccineDate) {
		this.vaccineDate = vaccineDate;
	}

	public String getNumberdoses() {
		return numberdoses;
	}

	public void setNumberdoses(String numberdoses) {
		this.numberdoses = numberdoses;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public String getCreatoruser() {
		return creatoruser;
	}

	public void setCreatoruser(String creatoruser) {
		this.creatoruser = creatoruser;
	}

	public String getModifieruser() {
		return modifieruser;
	}

	public void setModifieruser(String modifieruser) {
		this.modifieruser = modifieruser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

}