package com.kruger.inventario.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

/**
 * Entity de Usuario creacion de tablas y columnas
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */

@Entity
@Table(name = UserEntity.TABLE_NAME)
public class UserEntity {

	protected static final String TABLE_NAME = "USUARIO";
	protected static final String SEQUENCE = "SEQ_USUARIO";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	@Column(name = "id_usuario")
	private Long userId;
	@Column(name = "nombres")
	private String fisrtname;
	@Column(name = "apellido")
	private String lastname;
	@Column(name = "cedula")
	private String dni;
	@Column(name = "correo")
	private String email;
	@Column(name = "contraseña")
	private String password;
	@Column(name = "estado_vacuna")
	private Boolean status_vaccine;
	@Column(name = "fecha_nacimiento")
	@Temporal(TemporalType.TIMESTAMP)
	private Date birthday;
	@Column(name = "direccion")
	private String address;
	@Column(name = "telefono")
	private String phone;
	@Column(name = "fecha_creacion")
	private Date creationdate;
	@Column(name = "fecha_modificacion")
	private Date modificationdate;
	@Column(name = "usuario_creador")
	private String creatoruser;
	@Column(name = "usuario_modificador")
	private String modifieruser;
	@Column(name = "estado")
	private String status;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "user")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<UserRol> userRolList;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "user")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<VaccineEntity> vaccineList;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getFisrtname() {
		return fisrtname;
	}

	public void setFisrtname(String fisrtname) {
		this.fisrtname = fisrtname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getStatus_vaccine() {
		return status_vaccine;
	}

	public void setStatus_vaccine(Boolean status_vaccine) {
		this.status_vaccine = status_vaccine;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public String getCreatoruser() {
		return creatoruser;
	}

	public void setCreatoruser(String creatoruser) {
		this.creatoruser = creatoruser;
	}

	public String getModifieruser() {
		return modifieruser;
	}

	public void setModifieruser(String modifieruser) {
		this.modifieruser = modifieruser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<UserRol> getUserRolList() {
		return userRolList;
	}

	public void setUserRolList(List<UserRol> userRolList) {
		this.userRolList = userRolList;
	}

	public List<VaccineEntity> getVaccineList() {
		return vaccineList;
	}

	public void setVaccineList(List<VaccineEntity> vaccineList) {
		this.vaccineList = vaccineList;
	}

	@Override
	public String toString() {
		return "UserEntity [userId=" + userId + ", fisrtname=" + fisrtname + ", lastname=" + lastname + ", dni=" + dni
				+ ", email=" + email + ", status_vaccine=" + status_vaccine + ", password=" + password + ", birthday="
				+ birthday + ", address=" + address + ", phone=" + phone + ", creationdate=" + creationdate
				+ ", modificationdate=" + modificationdate + ", creatoruser=" + creatoruser + ", modifieruser="
				+ modifieruser + ", status=" + status + ", userRolList=" + userRolList + ", vaccineList=" + vaccineList
				+ "]";
	}

}
