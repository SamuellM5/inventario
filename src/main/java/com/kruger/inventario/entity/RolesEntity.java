package com.kruger.inventario.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.ForeignKey;

/**
 * Definición de la entidad Roles.
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2020
 */

@Entity
@Table(name = RolesEntity.TABLE_NAME)
public class RolesEntity {

	protected static final String TABLE_NAME = "ROLES";
	protected static final String SEQUENCE = "SEQ_ROLES";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	@Column(name = "id_rol")
	private Long rolId;
	@Column(name = "rol")
	private String rol;
	@Column(name = "codigo")
	private String code;
	@Column(name = "fecha_creacion")
	private Date creationdate;
	@Column(name = "fecha_modificacion")
	private Date modificationdate;
	@Column(name = "usuario_creador")
	private String creatoruser;
	@Column(name = "usuario_modificador")
	private String modifieruser;
	@Column(name = "estado")
	private String status;
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY, mappedBy = "rol")
	@Fetch(value = FetchMode.SUBSELECT)
	private List<UserRol> userRolList;

	public Long getRolId() {
		return rolId;
	}

	public void setRolId(Long rolId) {
		this.rolId = rolId;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public String getCreatoruser() {
		return creatoruser;
	}

	public void setCreatoruser(String creatoruser) {
		this.creatoruser = creatoruser;
	}

	public String getModifieruser() {
		return modifieruser;
	}

	public void setModifieruser(String modifieruser) {
		this.modifieruser = modifieruser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<UserRol> getUserRolList() {
		return userRolList;
	}

	public void setUserRolList(List<UserRol> userRolList) {
		this.userRolList = userRolList;
	}

}
