package com.kruger.inventario.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Definición de la entidad Usuario Rol creacion de tablas y columnas
 * 
 * @author Samuel Moreira Rodriguez <mailto:samuelmoreira@hotmail.es>
 * @version 1.0
 * @since 13/10/2021
 *
 */
@Entity
@Table(name = UserRol.TABLE_NAME)
public class UserRol {

	protected static final String TABLE_NAME = "USUARIO_ROL";
	protected static final String SEQUENCE = "SEQ_USUARIO_ROL";

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = SEQUENCE)
	@SequenceGenerator(name = SEQUENCE, sequenceName = SEQUENCE, allocationSize = 1)
	@Column(name = "id_usuario_rol")
	private Long userRolId;

	@Column(name = "fecha_creacion")
	private Date creationdate;
	@Column(name = "fecha_modificacion")
	private Date modificationdate;
	@Column(name = "usuario_creador")
	private String creatoruser;
	@Column(name = "usuario_modificador")
	private String modifieruser;
	@Column(name = "estado")
	private String status;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "FK_USUARIO_USUARIO_ROLES"), nullable = true)
	private UserEntity user;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "rol_id", foreignKey = @ForeignKey(name = "FK_USUARIO_ROLES_ROLES"), nullable = true)
	private RolesEntity rol;

	public Long getUserRolId() {
		return userRolId;
	}

	public void setUserRolId(Long userRolId) {
		this.userRolId = userRolId;
	}

	public Date getCreationdate() {
		return creationdate;
	}

	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}

	public Date getModificationdate() {
		return modificationdate;
	}

	public void setModificationdate(Date modificationdate) {
		this.modificationdate = modificationdate;
	}

	public String getCreatoruser() {
		return creatoruser;
	}

	public void setCreatoruser(String creatoruser) {
		this.creatoruser = creatoruser;
	}

	public String getModifieruser() {
		return modifieruser;
	}

	public void setModifieruser(String modifieruser) {
		this.modifieruser = modifieruser;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public RolesEntity getRol() {
		return rol;
	}

	public void setRol(RolesEntity rol) {
		this.rol = rol;
	}

}
