# Inventario

Proyecto **Inventario** 
# Requisitos

* java version 8
* maven 3.6.3

# Parametros de configuración

***aplication.properties***

* spring.application.name=xxxxxxxx
* server.port=XXXX
* server.servlet.context-path=/xxxxx/xxxx/xxxx

# Levantar Contenedro Docker

seguir los siguientes puntos

- Tener instalado Docker
- Tener instalado Docker Compose
- Ruta de ubicacion del jar
- Ruta donde se encuentra el archivo de conf `application.properties` copiarlo en el directorio src/main/resources/)

tomar el archivo `Dockerfile` y el archivo  `docker-compose.yml`, el cual esta detallado las configuraciones necesarias para que se levante el proyecto en contenedor docker.

Ejecutamos cualquier de los siguientes comando en cada escenario:

- ejecutar el comando `docker-compose up`
- ejecutar el comando `docker-compose up --build`
.

# Swagger

***URL***

```
http://localhost:8080/swagger-ui.html#/
```


